# yo-dwm
## DWM but cleaner and more usable

### Applayed patches:
- [alwayscenter](https://dwm.suckless.org/patches/alwayscenter)
- [cool autostart](https://dwm.suckless.org/patches/cool_autostart)
- [fullscreen](https://dwm.suckless.org/patches/fullscreen)
- [gridmode](https://dwm.suckless.org/patches/gridmode)
- [movestack](https://dwm.suckless.org/patches/movestack)
- [pertag](https://dwm.suckless.org/patches/pertag)
- [systray](https://dwm.suckless.org/patches/systray)
- [sticky](https://dwm.suckless.org/patches/sticky)
- [noborder](https://dwm.suckless.org/patches/noborder)
- [fibonacci layouts](https://dwm.suckless.org/patches/fibonacci)
- [centeredmaster](https://dwm.suckless.org/patches/centeredmaster)
- [keychain](https://dwm.suckless.org/patches/keychain)

### Dependencies
- `libxinerama-dev`
- `libxft-bgra` for color emojies support
- `yo-slstatus` the status bar

### Available in
[![GitHub](https://img.shields.io/badge/GitHub-Main%20repo-brightgreen?style=for-the-badge&logo=GitHub)](https://github.com/anas-elgarhy/dwm-anas)
[![GitLab](https://img.shields.io/badge/GitLab-Mirror%20repo-brightgreen?style=for-the-badge&logo=GitLab)](https://gitlab.com/anas-elgarhy/dwm-anas)
[![BitBucket](https://img.shields.io/badge/BitBucket-Mirror%20repo-brightgreen?style=for-the-badge&logo=BitBucket)](https://bitbucket.org/anas_elgarhy/dwm-anas)
[![Codeberg](https://img.shields.io/badge/Codeberg-Mirror%20repo-brightgreen?style=for-the-badge&logo=Codeberg)](https://codeberg.org/anas-elgarhy/dwm-anas)

### Refrences:
- [XF86keysym](https://cgit.freedesktop.org/xorg/proto/x11proto/tree/XF86keysym.h)
